This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# multiple global state reducer

## Create global Provider and hook

```jsx
function reducer(state, action) {
  switch (action.type) {
    case 'inc':
      return state + 1;
    case 'dec':
      return state - 1;
    default:
      return state;
  }
}

const initialValue = 10;

export const [CounterProvider, useGlobalCounter] = createGlobalReducer(
  reducer,
  initialValue
);
```

## Using Provider

```jsx
import { CounterProvider } from './hooks';

export default () => (
  <CounterProvider>
    <App />
  </CounterProvider>
);
```

## Using global hook

```jsx
import { useGlobalCounter } from './hooks';

export default function Counter({ name }) {
  const [state, dispatch] = useGlobalCounter();

  const increment = useCallback(() => dispatch({ type: 'inc' }), [dispatch]);
  const decrement = useCallback(() => dispatch({ type: 'dec' }), [dispatch]);

  return (
    <div>
      <h2>{name}</h2>
      {state}
      <button onClick={increment}>+</button>
      <button onClick={decrement}>-</button>
    </div>
  );
}
```
