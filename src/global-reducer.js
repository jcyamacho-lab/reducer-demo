import React, {
  useCallback,
  useEffect,
  useContext,
  createContext,
  useState,
  useMemo
} from 'react';

export function createGlobalReducer(
  reducer,
  initialState = undefined,
  actionsFactory = null
) {
  const Context = createContext(initialState);
  const handlers = [];

  function useGlobalReducer() {
    const state = useContext(Context);
    const dispatch = useCallback(
      action => {
        const newState = reducer(state, action);
        handlers.forEach(handler => handler(newState));
      },
      [state]
    );

    const actions = useMemo(() => {
      if (typeof actionsFactory === 'function') {
        return actionsFactory(dispatch, state);
      }
      return {};
    }, [state, dispatch]);

    return [state, dispatch, actions];
  }

  const Provider = ({ children }) => {
    const [value, setValue] = useState(initialState);
    useEffect(() => {
      handlers.push(setValue);
      return () => {
        const index = handlers.indexOf(setValue);
        handlers.splice(index, 1);
      };
    }, [setValue]);
    return <Context.Provider value={value}>{children}</Context.Provider>;
  };

  return [Provider, useGlobalReducer];
}

export function createReducer(map = {}) {
  return function reducer(state, action) {
    const handler = map[action.type];
    if (typeof handler === 'function') {
      return handler(state, action);
    }
    return state;
  };
}
